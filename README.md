Hi 👋 My name is Carlton Agesa
==============================

Full-stack website developer |Ruby & Ruby on Rails | JavaScript | React |
--------------------------------------------------

* 🌍  I'm based in Nairobi, Kenya
* 🖥️  See my portfolio at [https://carltonagesa.netlify.app/](http://carltonagesa.netlify.app/)
* ✉️  You can contact me at [carltonagesa@gmail.com](mailto:carltonagesa@gmail.com)
* 🧠  I'm learning React JavaScript
* 🤝  I'm open to collaborating on Github and other platforms

<a href="https://www.twitter.com/carl_agesa" target="_blank" rel="noreferrer"><img
src="https://img.shields.io/twitter/follow/carl_agesa?logo=twitter&style=for-the-badge&color=0891b2&labelColor=1e3a8a"
/></a><a href="https://www.github.com/carlagesa" target="_blank" rel="noreferrer"><img
src="https://img.shields.io/github/followers/carlagesa?logo=github&style=for-the-badge&color=0891b2&labelColor=1e3a8a" /></a>

### Skills

<p align="left">
  
  <a href="https://www.ruby-lang.org/en/" target="_blank" rel="noreferrer"><img src="https://raw.githubusercontent.com/danielcranney/readme-generator/main/public/icons/skills/ruby-colored.svg" width="36" height="36" alt="Ruby" /></a>
</p>

<a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript" target="_blank" rel="noreferrer"><img src="https://raw.githubusercontent.com/danielcranney/readme-generator/main/public/icons/skills/javascript-colored.svg" width="36" height="36" alt="JavaScript" /></a>
<a href="https://developer.mozilla.org/en-US/docs/Glossary/HTML5" target="_blank" rel="noreferrer"><img src="https://raw.githubusercontent.com/danielcranney/readme-generator/main/public/icons/skills/html5-colored.svg" width="36" height="36" alt="HTML5" /></a>
<a href="https://reactjs.org/" target="_blank" rel="noreferrer"><img src="https://raw.githubusercontent.com/danielcranney/readme-generator/main/public/icons/skills/react-colored.svg" width="36" height="36" alt="React" /></a>
<a href="https://www.w3.org/TR/CSS/#css" target="_blank" rel="noreferrer"><img src="https://raw.githubusercontent.com/danielcranney/readme-generator/main/public/icons/skills/css3-colored.svg" width="36" height="36" alt="CSS3" /></a>
<a href="https://getbootstrap.com/" target="_blank" rel="noreferrer"><img src="https://raw.githubusercontent.com/danielcranney/readme-generator/main/public/icons/skills/bootstrap-colored.svg" width="36" height="36" alt="Bootstrap" /></a>
<a href="https://www.adobe.com/uk/products/photoshop.html" target="_blank" rel="noreferrer"><img src="https://raw.githubusercontent.com/danielcranney/readme-generator/main/public/icons/skills/photoshop-colored-dark.svg" width="36" height="36" alt="Photoshop" /></a>
<a href="adobe.com/uk/products/illustrator.html" target="_blank" rel="noreferrer"><img src="https://raw.githubusercontent.com/danielcranney/readme-generator/main/public/icons/skills/illustrator-colored-dark.svg" width="36" height="36" alt="Illustrator" /></a>
<a href="https://www.figma.com/" target="_blank" rel="noreferrer"><img src="https://raw.githubusercontent.com/danielcranney/readme-generator/main/public/icons/skills/figma-colored.svg" width="36" height="36" alt="Figma" /></a>
</p>


### Socials

<p align="left"> <a href="https://www.github.com/carl_agesa" target="_blank" rel="noreferrer"><img src="https://raw.githubusercontent.com/danielcranney/readme-generator/main/public/icons/socials/github-dark.svg" width="32" height="32" /></a> <a href="https://www.linkedin.com/in/carlton-agesa" target="_blank" rel="noreferrer"><img src="https://raw.githubusercontent.com/danielcranney/readme-generator/main/public/icons/socials/linkedin.svg" width="32" height="32" /></a> <a href="https://www.twitter.com/carl_agesa" target="_blank" rel="noreferrer"><img src="https://raw.githubusercontent.com/danielcranney/readme-generator/main/public/icons/socials/twitter.svg" width="32" height="32" /></a></p>

### Badges

<b>My GitHub Stats</b>

<a href="http://www.github.com/carlagesa"><img src="https://github-readme-stats.vercel.app/api?username=carlagesa&show_icons=true&hide=&count_private=true&title_color=0891b2&text_color=ffffff&icon_color=0891b2&bg_color=1e3a8a&hide_border=true&show_icons=true" alt="carlagesa's GitHub stats" /></a>

<a href="http://www.github.com/carlagesa"><img src="https://github-readme-streak-stats.herokuapp.com/?user=carlagesa&stroke=ffffff&background=1e3a8a&ring=0891b2&fire=0891b2&currStreakNum=ffffff&currStreakLabel=0891b2&sideNums=ffffff&sideLabels=ffffff&dates=ffffff&hide_border=true" /></a>

<a href="http://www.github.com/carlagesa"><img src="https://activity-graph.herokuapp.com/graph?username=carlagesa&bg_color=1e3a8a&color=ffffff&line=0891b2&point=ffffff&area_color=1e3a8a&area=true&hide_border=true&custom_title=GitHub%20Commits%20Graph" alt="GitHub Commits Graph" /></a>

<a href="https://github.com/carlagesa" align="left"><img src="https://github-readme-stats.vercel.app/api/top-langs/?username=carlagesa&langs_count=10&title_color=0891b2&text_color=ffffff&icon_color=0891b2&bg_color=1e3a8a&hide_border=true&locale=en&custom_title=Top%20%Languages" alt="Top Languages" /></a>

<b>Top Repositories</b>

<div width="100%" align="center"><a href="https://github.com/carlagesa/Watchflix" align="left"><img align="left" width="45%" src="https://github-readme-stats.vercel.app/api/pin/?username=carlagesa&repo=Watchflix&title_color=0891b2&text_color=ffffff&icon_color=0891b2&bg_color=1e3a8a&hide_border=true&locale=en" /></a></div><br /><br /><br /><br /><br /><br /><br />
